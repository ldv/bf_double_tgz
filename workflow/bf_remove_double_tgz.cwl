cwlVersion: v1.2
class: Workflow
inputs:
  - id: bf_tar_archive
    doc: Tar archive with the BeamFormed dataset
    type: File
  - id: pulp_log_folder
    type: Directory

outputs:
  - id: tar_archive
    type: File
    outputSource: eliminate_double_tgz/output_tar
  - id: ingest
    type: Any
    outputSource: format_ingest/ingest
  - id: summary
    type: Any
    outputSource: eliminate_double_tgz/summary
  - id: plots
    type: File[]
    outputSource: eliminate_double_tgz/plots
  - id: summary_file
    type: File
    outputSource: eliminate_double_tgz/summary_file

steps:
- id: eliminate_double_tgz
  run: ../steps/double_tgz_elimination.cwl
  in: 
  - id: source_tgz
    source: bf_tar_archive
  out:
  - output_tar
  - file_content
  - plots
  - summary
  - summary_file
- id: extract_metadata
  run: ../steps/extract_metadata.cwl
  in:
  - id: sas_id
    source: bf_tar_archive
    valueFrom: $(self.basename.match('L([0-9]+)')[1])
  - id: log_root_folder
    source: pulp_log_folder
  - id: input_tar
    source: bf_tar_archive
  out:
  - id: output_json
- id: compute_md5sum
  run: ../steps/computemd5.cwl
  in:
  - id: archive
    source: eliminate_double_tgz/output_tar
  out:
  - id: md5sum
- id: format_ingest
  run: ../steps/format_ingest.cwl
  in:
  - id: metadata
    source: extract_metadata/output_json
  - id: file_content
    source: eliminate_double_tgz/file_content
  - id: output_name
    default: tar_archive
  - id: md5sum
    source: compute_md5sum/md5sum
  - id: file_name
    source: eliminate_double_tgz/output_tar
    valueFrom: $(self.basename)
  - id: filesize
    source: eliminate_double_tgz/output_tar
    valueFrom: $(self.size)
  out:
  - id: ingest
requirements:
- class: StepInputExpressionRequirement
- class: InlineJavascriptRequirement
