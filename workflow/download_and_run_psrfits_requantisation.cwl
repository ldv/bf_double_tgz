#!/usr/bin/env cwl-runner

cwlVersion: v1.2
class: Workflow

requirements:
- class: InlineJavascriptRequirement
- class: ScatterFeatureRequirement
- class: SubworkflowFeatureRequirement

inputs:
- id: surls
  type: string[]

outputs:
- id: tar_archive
  type: File[]
  outputSource: requantise/tar_archive
- id: summary
  type: Any[]
  outputSource: requantise/summary
- id: summary_file
  type: File[]
  outputSource: requantise/summary_file
- id: ingest
  type: Any
  outputSource: requantise/ingest
- id: quality
  type: Any
  outputSource: format_quality/quality

steps:
- id: fetch_data
  in:
  - id: surl_link
    source: surls
  scatter: surl_link
  run: ../steps/fetch_data.cwl
  out:
  - id: tar_archive
- id: requantise
  in:
  - id: bf_tar_archive
    source: fetch_data/tar_archive
  scatter: bf_tar_archive
  run: psrfits_requantisation.cwl
  out:
  - id: summary_file
  - id: summary
  - id: tar_archive
  - id: ingest
  - id: plots
- id: join_contents
  in:
  - id: summary_files
    source: requantise/summary_file
  - id: metadata
    source: requantise/summary
  run: ../steps/join_contents.cwl
  out:
  - id: summary_file
- id: format_quality
  in:
  - id: summary_file
    source: join_contents/summary_file
  - id: plots
    source: requantise/plots
  - id: summaries
    source: requantise/summary
  run: ../steps/format_quality.cwl
  out:
  - id: quality
