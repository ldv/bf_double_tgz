#!/usr/bin/env cwl-runner

cwlVersion: v1.2
class: Workflow

requirements:
- class: StepInputExpressionRequirement
- class: InlineJavascriptRequirement

inputs:
- id: bf_tar_archive
  doc: Tar archive with the BeamFormed dataset
  type: File

outputs:
- id: tar_archive
  type: File
  outputSource: requantise/bf_archive
- id: summary
  type: Any
  outputSource: requantise/summary
- id: summary_file
  type: File
  outputSource: requantise/summary_file
- id: plots
  type: File[]
  outputSource: requantise/plots
- id: ingest
  type: Any
  outputSource: format_ingest/ingest

steps:
- id: requantise
  in:
  - id: bf_tar_archive
    source: bf_tar_archive
  run: ../steps/requantize.cwl
  out:
  - id: file_content
  - id: summary
  - id: summary_file
  - id: bf_archive
  - id: plots

- id: compute_md5sum
  run: ../steps/computemd5.cwl
  in:
  - id: archive
    source: requantise/bf_archive
  out:
  - id: md5sum
- id: format_ingest
  run: ../steps/format_ingest.cwl
  in:
  - id: file_content
    source: requantise/file_content
  - id: output_name
    default: tar_archive
  - id: md5sum
    source: compute_md5sum/md5sum
  - id: file_name
    source: requantise/bf_archive
    valueFrom: $(self.basename)
  - id: filesize
    source: requantise/bf_archive
    valueFrom: $(self.size)
  out:
  - id: ingest
