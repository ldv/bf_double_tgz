#!/usr/bin/env cwl-runner

cwlVersion: v1.2
class: Workflow

requirements:
- class: ScatterFeatureRequirement
- class: SubworkflowFeatureRequirement

inputs:
- id: surls
  type: string[]
- id: pulp_log_folder
  doc: Pulp Log Folder
  type: Directory
  default:
    class: Directory
    path: /project/ldv/Data/beamformed/pulp-logs/

outputs:
- id: tar_archive
  type: File[]
  outputSource: bf_process/tar_archive
- id: ingest
  type: Any
  outputSource: bf_process/ingest
- id: summary
  type: string[]
  outputSource: bf_process/summary
- id: quality
  type: Any
  outputSource: format_quality/quality

steps:
- id: fetch_data
  in:
  - id: surl_link
    source: surls
  scatter: surl_link
  run: ../steps/fetch_data.cwl
  out:
  - id: tar_archive
- id: bf_process
  in:
  - id: bf_tar_archive
    source: fetch_data/tar_archive
  - id: pulp_log_folder
    source: pulp_log_folder
  scatter: bf_tar_archive
  run: ./bf_remove_double_tgz.cwl
  out:
  - id: tar_archive
  - id: ingest
  - id: summary
  - id: plots
  - id: summary_file
- id: join_contents
  in:
  - id: summary_files
    source: bf_process/summary_file
  - id: metadata
    source: bf_process/summary
  run: ../steps/join_contents.cwl
  out:
  - id: summary_file
- id: format_quality
  in:
  - id: summary_file
    source: join_contents/summary_file
  - id: plots
    source: bf_process/plots
  - id: summaries
    source: bf_process/summary
  run: ../steps/format_quality.cwl
  out:
  - id: quality
