#!/usr/bin/env python
#
import datetime as dt
import sys
from datetime import datetime


# populating observation info
def populating_observation(sasid, observation, parsetlines, loglines):
    # getting Project
    # It exists for Unspecified Process as well
    try:
        res = [ii for ii in loglines if "Project:" in ii]
        project = res[-1].split("Project:", 1)[-1].split("PI:", 1)[0].strip()
        observation["Project"] = project
    except:
        print("(E) Bad logfile for SASid %s. Pipelines has probably failed" % (sasid))
        sys.exit(1)

    # Creator - it is always AWTIER0  ??
    # It exists for Unspecified Process as well
    observation["Creator"] = "AWTIER0"

    # Privileges - is it always - 4?
    # It exists for Unspecified Process as well
    observation["Privileges"] = 4

    # Release Date
    # It exists for Unspecified Process as well
    # should be read from Unspecified Process
    observation["Release Date"] = ""

    # SAS Id
    # It exists for Unspecified Process as well
    # should be read from Unspecified Process
    observation["SAS Id"] = str(sasid)

    # SAS Id Source
    observation["SAS Id Source"] = "SAS"

    # Process Identifier Name
    res = [ii for ii in loglines if "Target" in ii]
    target = res[0].split("Target: ")[1].split(" ")[0].strip()
    observation["Process Identifier Name"] = target

    # Process Identifier Source, always MoM before?
    observation["Process Identifier Source"] = "MoM"

    # Observing Mode
    observation["Observing Mode"] = "Beam Observation"  # always like that?

    # Antenna Set
    res = [ii for ii in loglines if "Band:" in ii]
    antenna = res[0].split("Band:")[0].strip()
    if (antenna.find("_") != -1):
        (an1, an2) = antenna.split("_")
        antenna = an1 + " " + an2[0] + an2[1:].lower()
    observation["Antenna Set"] = antenna

    # Instrument Filter
    res = [ii for ii in loglines if "Band:" in ii]
    filter = res[0].split("Band:")[1].split("Mode:")[0].strip()
    filter = filter.replace("_", "-") + " MHz"
    observation["Instrument Filter"] = filter

    # Channel Width and Nr. Channels per Sub
    res = [ii for ii in loglines if "SubWidth:" in ii]
    subwidth = float(res[0].split("SubWidth:")[1].split("kHz")[0].strip()) / 1000.  # in MHz
    res = [ii for ii in loglines if "chans/sub:" in ii]
    nchan_per_sub = int(res[0].split("/sub:")[1].split("Downsample")[0].strip())
    if nchan_per_sub == 0:
        if len(parsetlines) > 0:
            res = [ii for ii in parsetlines if "Observation.channelsPerSubband" in ii]
            nchan_per_sub = int(res[0].split("=")[1].strip())
        else:
            print("(W) Parset file is not available for SASid %s" % (sasid))
            nchan_per_sub = 1

    chanwidth = subwidth / nchan_per_sub
    observation["Channel Width [MHz]"] = chanwidth
    observation["Channels Per Subband"] = nchan_per_sub

    # Number of SubArray Pointings
    res = [ii for ii in loglines if "SAPs:" in ii]
    nsaps = int(res[0].split("SAPs:")[1].split("Target")[0].strip())
    observation["Number of SubArray Pointings"] = nsaps

    # Number of subbands (not in the metadata for observation - only for the specific part of a beam)
    # I collect this now for testing purposes to calculate #nsubs * #nsaps - total number of subbands, to see
    # if this number > 244, or not (to determine if 8-bit was used or not)
    res = [ii for ii in loglines if "subbands:" in ii]
    nsubs = int(res[0].split(":")[1].split("[")[0].strip())
    # observation["Total number of subbands"] = nsubs # * nsaps (NOT needed, as in the logfile is already the total number of subs)

    # Start Time
    res = [ii for ii in loglines if "Start UTC:" in ii]
    starttime = res[0].split("UTC:")[1].split("Duration")[0].strip()
    observation["Start Time"] = starttime

    # Duration [s]
    res = [ii for ii in loglines if "Start UTC:" in ii]
    dur = res[0].split("Duration:")[1].strip()

    if dur[-1] == "s":
        duration = float(dur.split("s")[0])
    elif dur[-1] == "m":
        duration = float(dur.split("m")[0]) * 60.
    elif dur[-1] == "h":
        duration = float(dur.split("h")[0]) * 3600.
    else:
        duration = 0
    observation["Duration [s]"] = duration

    # End Time
    # 2014-05-02 06:21:00.000000000  get rid of the usec in the starttime string
    st = datetime.strptime(starttime.rsplit(".")[0], '%Y-%m-%d %H:%M:%S')
    et = st + dt.timedelta(0, duration)
    endtime = et.strftime('%Y-%m-%d %H:%M:%S')
    observation["End Time"] = endtime

    # Bits Per Sample (reading parset file)
    if len(parsetlines) > 0:
        res = [ii for ii in parsetlines if "Observation.nrBitsPerSample" in ii]
        if len(res) != 0:
            observation["Bits Per Sample"] = int(res[0].split("=")[1].strip())
        else:
            res = [ii for ii in parsetlines if "OLAP.nrBitsPerSample" in ii]
            if len(res) != 0:
                observation["Bits Per Sample"] = int(res[0].split("=")[1].strip())
            else:
                res = [ii for ii in parsetlines if
                       "Observation.ObservationControl.OnlineControl.OLAP.nrBitsPerSample" in ii]
                if len(res) != 0:
                    observation["Bits Per Sample"] = int(res[0].split("=")[1].strip())
                else:
                    if nsubs > 244:
                        observation["Bits Per Sample"] = 8
                    else:
                        observation["Bits Per Sample"] = 16
    else:  # if parsetfile is not available we will use the number of subs to guestimate
        if nsubs > 244:
            observation["Bits Per Sample"] = 8
        else:
            observation["Bits Per Sample"] = 16

    # Observing Description
    observation["Observing Description"] = "%s, %s, %s, %d bit, %d ch/sub, Nsubs=%d" % (
        antenna, filter, dur, observation["Bits Per Sample"], nchan_per_sub, nsubs)

    # Clock
    res = [ii for ii in loglines if "Clock:" in ii]
    clock = res[-1].split("Clock:")[1].split("MHz")[0].strip()
    observation["Clock [MHz]"] = clock

    # Station Selection - is it always 'Custom'?
    observation["Station Selection"] = "Custom"

    # Number of stations (core, remote, international)
    res = [ii for ii in loglines if "stations:" in ii]
    nstat = int(res[-1].split("stations:")[1].split("[")[0].strip())
    ncore = int(res[-1].split("[")[1].split("CS")[0].strip())
    nremote = int(res[-1].split(",")[1].split("RS")[0].strip())
    ninternational = nstat - ncore - nremote
    observation["Number of Stations"] = nstat
    observation["Nr Stations Core"] = ncore
    observation["Nr Stations Remote"] = nremote
    observation["Nr Stations International"] = ninternational

    # Number of Correlated DataProducts - always 0?
    observation["Number of Correlated DataProducts"] = 0

    # Number of Transient BufferBoard Events - is it always -1?
    observation["Number of Transient BufferBoard Events"] = -1

    # Strategy Name - always BeamObservation?
    observation["Strategy Name"] = "BeamObservation"

    # Strategy Description - it it always "default"?
    observation["Strategy Description"] = "default"

    # Time System - is it always "UTC"?
    observation["Time System"] = "UTC"

    # Number of BeamFormed DataProducts
    res = [ii for ii in parsetlines if "Observation.DataProducts.Output_Beamformed.filenames" in ii]
    if len(res) > 0:
        temp = res[-1].split("=")[1].strip().split("[")[1].split("]")[0].strip()
        if len(temp) == 0:
            nbeamfiles = 0
        else:
            nbeamfiles = len(temp.split(","))
        observation["Number of BeamFormed DataProducts"] = nbeamfiles
    else:
        res1 = [ii for ii in parsetlines if "Observation.DataProducts.Output_CoherentStokes.filenames" in ii]
        res2 = [ii for ii in parsetlines if "Observation.DataProducts.Output_IncoherentStokes.filenames" in ii]
        if len(res1) + len(res2) > 0:
            temp = res1[-1].split("=")[1].strip().split("[")[1].split("]")[0].strip()
            if len(temp) == 0:
                ncsfiles = 0
            else:
                ncsfiles = len(temp.split(","))
            temp = res2[-1].split("=")[1].strip().split("[")[1].split("]")[0].strip()
            if len(temp) == 0:
                nisfiles = 0
            else:
                nisfiles = len(temp.split(","))
            observation["Number of BeamFormed DataProducts"] = ncsfiles + nisfiles
        else:
            observation["Number of BeamFormed DataProducts"] = 0

    return observation
