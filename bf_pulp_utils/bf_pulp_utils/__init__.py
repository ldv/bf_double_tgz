from importlib.metadata import version, PackageNotFoundError

try:
    __version__ = version("bf_pulp_utils")
except PackageNotFoundError:
    # package is not installed
    pass