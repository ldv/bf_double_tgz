#!/usr/bin/env python
#
# Collecting metadata for unspecified LTA pipeline data products (and corresponding observations)
# based on PulP log- and feedback files
#
# (c) Vlad Kondratiev - 01.11.2021
#
import glob
import json
import os
import re
import sys
import tarfile
from argparse import ArgumentParser

from bf_pulp_utils.ldv_obs import *
from bf_pulp_utils.ldv_pulp import *
import fnmatch

# directory with all PulP log- and feedback files
# when executed on Spider in folder '/project/ldv/Data/beamformed/' the path is relative
DEFAULT_ROOTDIR = "./pulp-logs"



def main():
    # dictionaries
    observation = {}
    pulp = {}

    # to save to JSON file
    global_dict = {
        "Observation": observation,
        "Pulsar Pipeline": pulp
    }
    # suffix for the filename of the output JSON file
    json_filename_suffix = "_unspecified.json"

    parser = ArgumentParser(description='Collect metadata from pulp-log files')
    parser.add_argument('obs_id')
    parser.add_argument('filename')
    parser.add_argument('--rootdir', help="Specify rootdir (absolute path)")
    args = parser.parse_args()

    if args.rootdir is None:
        print("No rootdir as option is given, use default path")
        rootdir = DEFAULT_ROOTDIR
    else:
        rootdir = args.rootdir

    parsetsdir = os.path.join(rootdir, 'parsets')

    # TODO use are argument for this like obs_id is 'all' ??
    # if ObsID is not given, then will be collecting all ObsIDs from the corresponding directories in the "root" directory
    # for now will be printing the message and exiting...

    sasid = args.obs_id

    # checking if directory <ObsID> exists
    if not os.path.isdir("%s/L%s" % (rootdir, sasid)):
        print("FATAL ERROR: No L%s directory in %s/" % (sasid, rootdir))
        sys.exit(1)
    else:
        logfile = "%s/L%s/L%s_pulp.log" % (rootdir, sasid, sasid)
        if not os.path.exists(logfile):
            print("WARNING: Log-file '%s' is not found" % logfile)
            logfile = "%s/L%s/pipeline.log" % (rootdir, sasid)
            print("   Will try the new-format log-file '%s'..." % logfile)
            if not os.path.exists(logfile):
                print("WARNING: Log-file '%s' is not found" % logfile)
                logfile = "%s/L%s/L%s_combined.log" % (rootdir, sasid, sasid)
                print("   OK... will then try the old-format log-file '%s'..." % logfile)
                if not os.path.exists(logfile):
                    print("FATAL ERROR: Log-file '%s' is also not found" % logfile)
                    sys.exit(1)

        # feedback file
        feedbackfile = "%s/Observation%s_feedback" % (rootdir, sasid)
        if not os.path.exists(feedbackfile):
            feedbackfile = "%s/L%s/pulpL%s_feedback" % (rootdir, sasid, sasid)
            if not os.path.exists(feedbackfile):
                print("WARNING: Feedback file is not found (neither '%s/Observation%s_feedback' nor '%s')" % (
                    rootdir, sasid, feedbackfile))
                print("   Will try to extact necessary info about the datatype of the dataproduct from the log-file...")
                feedbackfile = "missing"

    # checking if parset file exists
    foundparsets = glob.glob("%s/*%s*" % (parsetsdir, sasid))

    parset = ""
    if len(foundparsets) > 0:
        parset = sorted(foundparsets, key=len)[0]

    # reading log-file file into the list of lines
    f = open(logfile, 'r')
    # ignoring empty lines
    # comments_and_empty=re.compile(r"(^\s*#+.*$)|(^\s*$)")
    empty = re.compile(r"(^\s*$)")
    if "pipeline" not in logfile:
        loglines = [ff for ff in f.read().splitlines() if empty.search(ff) is None]
    else:
        loglines = [ff.split("pulsar_pipeline: ")[-1] for ff in f.read().splitlines() if empty.search(ff) is None]
    f.close()
    # reading feedback-file file into the list of lines
    if feedbackfile != "missing":
        f = open(feedbackfile, 'r')
        empty = re.compile(r"(^\s*$)")
        feedlines = [ff for ff in f.read().splitlines() if empty.search(ff) is None]
        f.close()
    else: # determining the type of the dataproduct from the log-file...
        if "LOFAR_PULSAR_ARCHIVE" in args.filename:
            tartype=args.filename.split("_")[5]
        else:
            tartype=args.filename.split("_")[1]

        datatype=""
        if "summaryCV" in tartype or "CVplots" in tartype:
            datatype="SummaryComplexVoltages"
        if "summaryCS" in tartype or "CSplots" in tartype:
            datatype="SummaryCoherentStokes"
        if "summaryIS" in tartype or "redIS" in tartype:
            datatype="SummaryIncoherentStokes"

        # will read the log-file to extract info for datatype, only if it's not a summary tarball
        if datatype == "":
            if "combined" not in logfile:
                if "SAP" in tartype:  # then the filename is new already
                    (sasidstr, sap, beam, part, rest) = args.filename.split("_")
                    sapint = int(sap.split("SAP")[-1])
                    if "BEAM" in beam:
                        beamint = int(beam.split("BEAM")[-1])
                    else:
                        beamint = int(beam.split("B")[-1])
                else:
                    if "red" in tartype:
                        inputtar = tarfile.open(args.filename, "r")
                        dircontent = [ii.name for ii in inputtar.getmembers()]
                        inputtar.close()
                        logf=""
                        for filename in fnmatch.filter(dircontent, '*.log'):
                            if "sap" in filename and "beam" in filename:
                                logf=filename
                                break
                        if logf != "":
                            pieces = logf.split("_")
                            for pp in pieces:
                                if "sap" in pp:
                                    sapint = int(pp.split("sap")[1])
                                    sapstr = "SAP%03d" % (sapint)
                                if "beam" in pp:
                                    beamint = int(pp.split("beam")[1].split(".")[0])
                                    beamstr = "B%03d" % (beamint)
                        else: # logfile is not found          
                            for filename in fnmatch.filter(dircontent, '*.h5') + fnmatch.filter(dircontent, '*.raw') + fnmatch.filter(dircontent, '*.fits') + fnmatch.filter(dircontent, '*.ar') + fnmatch.filter(dircontent, '*.pfd*'):
                                if "_SAP" in filename and "_B" in filename:
                                    logf = filename
                                    break
                            if logf != "":
                                if "_P" not in logf:
                                    partint = 0
                                    partstr = "P000"
                                pieces = logf.split('/')[-1].split("_")
                                for pp in pieces:
                                    if "SAP" in pp:
                                        sapint = int(pp.split("SAP")[1])
                                        sapstr = "SAP%03d" % (sapint)
                                    if "B" in pp: 
                                        beamint = int(pp.split("B")[1])
                                        beamstr = "B%03d" % (beamint)
                                    if "_P" in logf:
                                        if "P" in pp: 
                                            partint = int(pp.split("P")[1])
                                            partstr = "P%03d" % (partint)
                            else: # logfile is not found again (possible reason is that it is VERY early data with 'RSP' in the file names
                                for filename in fnmatch.filter(dircontent, '*.out'):
                                    if "2bf2fits_" in filename:
                                        logf = filename
                                        break
                                if logf != "":
                                    # extracting this found logfile in order to get info
                                    inputtar = tarfile.open(args.filename, "r")
                                    members = inputtar.getmembers()
                                    log_to_delete=""
                                    for ii in members:
                                        if "2bf2fits_" in ii.name and ".out" in ii.name:
                                            inputtar.extract(ii, "./")
                                            log_to_delete = ii.name
                                            break
                                    inputtar.close()
                                    # reading log-file file into the list of lines
                                    f = open(logf, 'r')
                                    # ignoring empty lines
                                    # comments_and_empty=re.compile(r"(^\s*#+.*$)|(^\s*$)")
                                    empty = re.compile(r"(^\s*$)")
                                    loglines = [ff for ff in f.read().splitlines() if empty.search(ff) is None]
                                    f.close()
                                    if log_to_delete != "":
                                        os.remove(log_to_delete)
                                    # look for the line for the specific SAP and TAB
                                    res = [ii for ii in loglines if "_bf.raw" in ii]
                                    rawfile = res[0].split("/")[-1].split(" ")[0].strip()
                                    if "_P" not in rawfile:
                                        partint = 0
                                        partstr = "P000"
                                    pieces = rawfile.split("_")
                                    for pp in pieces:
                                        if "SAP" in pp:
                                            sapint = int(pp.split("SAP")[1])
                                            sapstr = "SAP%03d" % (sapint)
                                        if "B" in pp:
                                            beamint = int(pp.split("B")[1])
                                            beamstr = "B%03d" % (beamint)
                                        if "_P" in rawfile:
                                            if "P" in pp:
                                                partint = int(pp.split("P")[1])
                                                partstr = "P%03d" % (partint)
                                    new_output_name = "%s_%s_%s_%s_bf.tar" % (
                                        sasid, sapstr, beamstr, partstr)
                                else:
                                    print("FATAL ERROR: can't find the information about the SAP and BEAM for this dataproduct!")
                                    sys.exit(1)

                # look for the line for the specific SAP and TAB
                res = [ii for ii in loglines if "SAP=%d TAB=%d" % (sapint, beamint) in ii]
                mode = res[0].split("(")[1].split(")")[0].strip()
                if mode == "CV":
                    datatype="ComplexVoltages"
                if mode == "CS":
                    datatype="CoherentStokes"
                if mode == "IS":
                    datatype="IncoherentStokes"

            else: # for the *_combined.log logfile
                # look for the line for the specific SAP and TAB
                try:
                    res = [ii for ii in loglines if "SAP" in ii]
                    sap = "SAP%03d" % (int(res[0].split("_SAP")[1].split("_")[0]))
                    beam = "B%03d" % (int(res[0].split("_SAP")[1].split("_")[1].split("B")[1]))
                    # looking for datatype
                    res = [ii for ii in loglines if "2bf2fits" in ii]
                    if len(res) != 0:
                        if "-CS" in res[0]:
                            datatype="CoherentStokes"
                        if "-IS" in res[0]:
                            datatype="IncoherentStokes"
                    else:
                        res = [ii for ii in loglines if "Coherentstokes set to:" in ii]
                        if len(res) != 0:
                            if "true" in res[0]:
                                datatype="CoherentStokes"
                            else:
                                datatype="IncoherentStokes"
                        else:
                            print("FATAL ERROR: can not determine the data type!")
                            sys.exit(1)
                except:
                    print ("FATAL ERROR when parsing the log-file %s" % (logfile))
                    sys.exit(1)

        feedlines=[datatype]

    # reading parset-file file into the list of lines
    if parset != "":
        f = open(parset, 'r')
        empty = re.compile(r"(^\s*$)")
        parsetlines = [ff for ff in f.read().splitlines() if empty.search(ff) is None]
        f.close()
    else:
        parsetlines = []

    # populating observation info
    # will only do that for *_pulp.log files. For older *_combined.log files we will only populate pipeline dictionary
    # and observation info will be fully taken from MoM db including the duration of observation
    if "combined" not in logfile:
        observation = populating_observation(sasid, observation, parsetlines, loglines)
        observation["Parset"] = parset.split("/")[-1]  # removing the path

    # populating pipeline info
    pulp = populating_pipeline(sasid, args.filename, pulp, loglines, feedlines, feedbackfile, logfile)
    if "combined" not in logfile:
        pulp["Project"] = observation["Project"]
        pulp["Creator"] = observation["Creator"]
        pulp["Privileges"] = observation["Privileges"]
        pulp["Release Date"] = observation["Release Date"]
    else: # will be updated in the Archiver stage
        pulp["Project"] = ""
        pulp["Creator"] = "AWTIER0"
        pulp["Privileges"] = 4
        pulp["Release Date"] = ""

    # writing to JSON file
    outname = "L%s%s" % (sasid, json_filename_suffix)
    with open(outname, "w") as outfile:
        json.dump(global_dict, outfile)

    print("File %s created" % outname)


if __name__ == '__main__':
    main()
