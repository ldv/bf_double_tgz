#!/usr/bin/env python
#
# Converting PSRFITS files in BF LTA tarballs to 2/4 bits
#
# (c) Vlad Kondratiev - 23.04.2023 
#
import fnmatch
import json
import optparse as opt
import os
import re
import shutil
import sys
import tarfile
import subprocess, shlex
from subprocess import PIPE, STDOUT, Popen

# WE WILL CONVERT TO NBIT=...
nbit=2

SIZE_FACTORS = ["B", "kB", "MB", "GB", "TB", "PB", "EB"]


def size_to_string(size: int):
    for factor_index, label in enumerate(SIZE_FACTORS):
        factor = 1024 ** factor_index
        next_factor = 1024 * factor
        if size < next_factor:
            return "{:.2f} {}".format(size / factor, label).strip()
    else:
        return None

def comparison_summary(sasid, input_path, output_path, input_size, output_size, to_delete, to_extract, rfilevel, is_summary):
    ldv_summary = "{}-ldv-summary.json".format(sasid)
    ratio = output_size / input_size
    summary = {}
    summary['input_name'] = input_path.split('/')[-1]
    summary['output_name'] = output_path.split('/')[-1]
    summary['input_size'] = input_size
    summary['output_size'] = output_size
    summary['input_size_str'] = size_to_string(input_size)
    summary['output_size_str'] = size_to_string(output_size)
    summary['size_ratio'] = ratio
    summary['deleted'] = to_delete
    summary['added'] = to_extract
    summary['rfi_percent'] = rfilevel
    summary['is_summary'] = is_summary
    with open(ldv_summary, 'w') as fp:
        json.dump(summary, fp)

# writing out the summary comparison between old and new tarballs
def formatted_comparison_summary(sasid, input_tarball, new_output_name, input_size, output_size, to_delete, to_extract):

    in_filename = input_tarball.split('/')[-1]
    out_filename = new_output_name.split('/')[-1]

    if "summaryCS" in in_filename or "summaryIS" in in_filename or "summaryCV" in in_filename:
        prefix = in_filename.split(".tar")[0]
    else:
        prefix = in_filename.split("_P000")[0]
    ldv_summary = "%s-ldv-summary.log" % (prefix,)
    fsum = open(ldv_summary, "w")
    fsum.write("="*98+"\n")
    fsum.write(("  " + " | ".join([in_filename, out_filename])+ "  ").center(98, "*") + "\n")
    #fsum.write("-"*98+"\n")

    ratio = output_size / input_size
    factors = {10: "kB", 20: "MB", 30: "GB", 40: "TB"}
    for ii in factors.keys():
        if (float)(input_size) / 2 ** ii < 1:
            break
        else:
            input_factor = ii
    for ii in factors.keys():
        if (float)(output_size) / 2 ** ii < 1:
            break
        else:
            output_factor = ii
    fsum.write("=" * 75 + "\n")
    fsum.write("Old name: %s\n" % (in_filename))
    fsum.write("New name: %s\n" % (out_filename))
    fsum.write("=" * 75 + "\n")
    fsum.write("Old size: %15ld bytes (%5.1f %s)\n" % (
        input_size, (float)(input_size) / 2 ** input_factor, factors[input_factor]))
    fsum.write("New size: %15ld bytes (%5.1f %s)\n" % (
        output_size, (float)(output_size) / 2 ** output_factor, factors[output_factor]))
    fsum.write("Size Ratio (new/old): %.3f\n" % (ratio))
    fsum.write("=" * 75 + "\n")
    fsum.write("Files to delete from the old tarball (%d):\n" % (len(to_delete)))
    for ii in to_delete:
        fsum.write(ii + "\n")
    fsum.write("=" * 75 + "\n")
    fsum.write("Files to add to the new tarball (%d):\n" % (len(to_extract)))
    for ii in to_extract:
        fsum.write(ii + "\n")
    fsum.write("=" * 75 + "\n")
    fsum.write("NOTE: the size of the new output tarball could be different (smaller)\n")
    fsum.write("even if there were no files added/deleted, as there could be copies of\n")
    fsum.write("the same files in the input tarball. These duplicate files (if any) were removed\n")
    fsum.close()

# main
def main():
    usage = "Usage: %prog [options] <input LTA tarball> [output tarball]"
    cmdline = opt.OptionParser(usage)

    # reading cmd options
    (opts, args) = cmdline.parse_args()

    # check if input file is given
    if len(args) == 0:
        cmdline.print_usage()
        sys.exit(0)

    # input LTA tarball
    input_tarball = args[0]
    input_size = os.path.getsize(input_tarball)

    filename = os.path.basename(input_tarball)
    if "LOFAR_PULSAR_ARCHIVE" in filename:
        sasid = filename.split("_")[4]
    else:
        sasid = filename.split("_")[0] 

    # output tarball
    if len(args) < 2:
        output_tarball = input_tarball
    else:
        output_tarball = args[1]

    # getting all *.fits in the LTA tarball
    matches = []
    inputtar = tarfile.open(input_tarball, "r")
    dircontent = [ii.name for ii in inputtar.getmembers()]
    for filename in fnmatch.filter(dircontent, '*.fits'):
        matches.append(filename)
    inputtar.close()

    # making the tarball copy
    if output_tarball != input_tarball:
        shutil.copyfile(input_tarball, output_tarball)

    #ascii_name = output_tarball + "_filecontent.txt"
    #with open(ascii_name, "w") as outfile:
    #    outfile.write("\n".join(dircontent))

    to_add=[]
    to_delete=[]
    # getting current directory
    workdir=os.getcwd()

    rootdir="root"
    os.mkdir(rootdir) # create "root" directory in the current dir where all files will be extracted from the tar
    os.chdir(rootdir)
    # extracting all files from the output tarball
    print ("Extracting all files:")
    os.system("tar xvf ../%s" % (output_tarball))
    # converting extracted 8-bit PSRFITS files to 2- or 4-bit PSRFITS files
    if len(matches) != 0:
        print ("Converting to %d-bit..." % (nbit))
    for ii in matches:
        print (ii)
        options="-outbits %d -adjustlevels -numfiles 1" % (nbit)
        outputbase=ii.split(".fits")[0] + "_%dbit" % (nbit)
        logfile="%s_ldv_psrfits_requantisation.log" % (outputbase)
        cmd="psrfits_subband %s -o %s %s" % (options, outputbase, ii)
        print (cmd)
        try:
            #os.system(cmd)
            # removing the converted file if it's still there (for instance after the crash, etc).
            # otherwise psrfits_subband will not work - can't override the file if it exists already
            os.system("rm -f %s.fits" % (outputbase))
            outf = open(logfile, 'w')
            outf.write("=" * 100 + "\n")
            outf.write("Input PSRFITS file: %s\n" % (ii))
            outf.write("Command: %s\n" % (cmd))
            outf.write("=" * 100 + "\n")
            outf.close()
            proc = Popen(shlex.split(cmd), stdout=PIPE, stderr=STDOUT, cwd="%s/%s" % (workdir, rootdir), shell=False)
            (sout, serr) = proc.communicate()
            outf = open(logfile, 'a')
            outf.write(sout.decode("utf-8").replace("\r\n", "\n"))
            outf.close()
            for line in sout.decode("utf-8").split("\n"):
                print (line)
            if proc.poll() != 0:
                raise Exception
            # renaming new converted fitsfile to the original name
            print("Renaming %s_0001.fits to %s.fits" % (outputbase, outputbase))
            os.rename("%s_0001.fits" % (outputbase), "%s.fits" % (outputbase))
            # copying *_ldv_psrfits_requantisation.log to the workdir
            os.system("cp %s %s/" % (logfile, workdir))
            # adding new files to 'to_add' list and original file to 'to_delete' list
            to_add.append("%s.fits" % (outputbase))
            to_add.append(logfile)
            to_delete.append(ii)
            # removing the original PSRFITS file
            print ("Removing original 8-bit PSRFITS file %s" % (ii))
            os.remove(ii)
        except Exception as error:
            print ("=========Error has occurred: ========")
            print (str(error))
            sys.exit(1)

    print ("Creating new tarball %s..." % (output_tarball))
    os.system("tar cvf %s *" % (output_tarball))
    os.system("mv -f %s .." % (output_tarball))
    os.chdir("../")
    os.system("rm -rf %s" % (rootdir))

    # renaming the output tarball if needed
    output_size = os.path.getsize(output_tarball)
    if output_tarball != input_tarball:
        os.rename(output_tarball, input_tarball)
    output_size = os.path.getsize(input_tarball)

    # making the list of filecontent of the new tarball and save it in the separate ascii file
    outputtar = tarfile.open(input_tarball, "r")
    members = outputtar.getmembers()
    newtarcontent = [ii.name for ii in members]
    # and extracting diagnostic plots
    for ii in members:
        if "status.png" in ii.name or ii.name == "combined.png" or ".pfd.png" in ii.name or \
                "_rfifind.ps" in ii.name or "_diag.png" in ii.name or "_diag_pdmp.png" in ii.name or (
                "heatmap" in ii.name and ".png" in ii.name and not "th.png" in ii.name) or (
                "singlepulse" in ii.name and ".png" in ii.name and not ".ps" in ii.name) or \
                "_pdmp.ps" in ii.name:
            outputtar.extract(ii, "./")
            # if file is Postscript file we will be converting it to PNG (2 pngs in case of rfifind.ps as it's 2-page PS)
            if ".ps" in ii.name:
                os.system(
                    "convert -rotate 90 -background white -alpha remove -alpha off %s %s" % (
                        ii.name, ii.name.split(".ps")[0] + ".png"))
                os.remove(ii.name)
    # and extracting snippets of the log info from rfifind runs
    is_summary = False
    rfilevel = 0
    is_rfilog_found = False
    for ii in members:
        if ".rfiout" in ii.name:
            outputtar.extract(ii, "./")
            rfiname = ii.name.split(".rfiout")[0] + ".rfifind.log"
            os.rename(ii.name, rfiname)
            is_rfilog_found = True
            continue
    if not is_rfilog_found:
        for ii in members:
            if ".log" in ii.name:
                outputtar.extract(ii, "./")
                # reading log-file file into the list of lines
                f = open("%s" % (ii.name), 'r')
                # ignoring empty lines
                # comments_and_empty=re.compile(r"(^\s*#+.*$)|(^\s*$)")
                empty = re.compile(r"(^\s*$)")
                loglines = [ff for ff in f.read().splitlines() if
                            empty.search(ff) is None]
                f.close()
                # does this log-file has any info about rfifind?
                matches = [ff for ff in loglines if 'rfifind' in ff]
                rfilevel_string = [ff for ff in loglines if 'Number' in ff and 'bad' in ff and 'intervals' in ff]
                if len(rfilevel_string) != 0:
                    rfilevel=float(rfilevel_string[0].split("(")[-1].split("%")[0])
                rfiname = ""
                if len(matches) == 1 and "*_rfifind*" in matches[0]:
                    os.remove(ii.name)
                    continue
                rfiname = ""
                if len(matches) != 0:
                    try:
                        rfiname = ii.name.split(".log")[0] + ".rfifind.log"
                        rfiout = open(rfiname, 'w')
                        ind1 = \
                            [i for i, s in enumerate(loglines) if
                             'Creating RFI mask' in s][
                                0]
                        ind2 = ind1 + 2
                        rfiout.write("\n".join(loglines[ind1:ind2]))
                        ind1 = [i for i, s in enumerate(loglines) if
                                'Waiting for rfifind to finish' in s][0]
                        ind2 = [i for i, s in enumerate(loglines) if
                                '(rfifind) has finished' in s][0]
                        ind2 += 1
                        rfiout.write("\n".join(loglines[ind1:ind2]))
                        rfiout.close()
                    except:
                        rfiout.close()
                        os.remove(rfiname)
                    os.remove(ii.name)
                    continue
                else:
                    if "psrfits_requantisation" not in ii.name:
                        os.remove(ii.name)
    outputtar.close()
    # if the tarball is the summary tarball, then we set RFI level to 0
    if "summary" in input_tarball:
        rfilevel = 0
        is_summary = True

    #os.remove(output_tarball + "_filecontent.txt")
    ascii_name = input_tarball + "_filecontent.txt"
    with open(ascii_name, "w") as outfile:
        outfile.write("\n".join(newtarcontent))

    # writing out the summary comparison between old and new tarballs
    comparison_summary(sasid, input_tarball, input_tarball, input_size, output_size, to_delete, to_add, rfilevel, is_summary)

    formatted_comparison_summary(sasid, input_tarball, input_tarball, input_size, output_size, to_delete, to_add)

if __name__ == '__main__':
    main()
