#!/usr/bin/env python
#
import sys
from datetime import datetime


def set_on_path(obj, path, value):
    leaf = obj
    for item in path[:-1]:
        if item.strip() not in leaf:
            leaf[item.strip()] = {}
            leaf = leaf[item.strip()]
        else:
            leaf = leaf[item.strip()]
    leaf[path[-1].strip()] = value.strip()


def parse_feedback_lines(feedlines):
    parsed_log_lines = {}
    for line in feedlines:
        if line == '':
            continue
        key, value = line.split('=')
        path = key.split('.')
        set_on_path(parsed_log_lines, path, value)
    return parsed_log_lines


def get_path_to_nested_dict(obj, path):
    path_keys = path.split('.')
    leaf = obj
    for path_key in path_keys:
        leaf = leaf[path_key]
    return leaf


def match_filename(filename, value):
    if 'plots' in filename and 'summary' in value:
        return True
    elif 'plots' not in filename:
        return True
    else:
        return False


# populating pipeline info
def populating_pipeline(sasid, filename, pulp, loglines, feedlines, feedbackfile, logfile):
    if feedbackfile != "missing":
        parsed_feedback = parse_feedback_lines(feedlines)
        dataproducts = get_path_to_nested_dict(parsed_feedback, 'LOFAR.ObsSW.Observation.DataProducts')
        dataproduct = None
        dataproduct_key = None
        for key, value in dataproducts.items():
            if '[' not in key:
                continue
            if match_filename(filename, value['filename']):
                dataproduct = value
                dataproduct_key = key
                break
        pulp['dataType'] = dataproduct['datatype']

    else:
        pulp['dataType'] = feedlines[0]

    if "combined" not in logfile:
        # single-pulse on/off
        res = [ii for ii in loglines if "Single-pulse" in ii]
        if len(res) == 0:
            doSinglePulseAnalysis = 0
        else:
            doSinglePulseAnalysis = res[-1].split("=", 1)[-1].strip()
            if doSinglePulseAnalysis == "no":
                doSinglePulseAnalysis = 0
            else:
                doSinglePulseAnalysis = 1
        pulp["doSinglePulseAnalysis"] = doSinglePulseAnalysis

        # convert raw data to 8 bit?
        res = [ii for ii in loglines if "RAW DATA" in ii]
        if len(res) == 0:
            convertRawTo8bit = 0
        else:
            convertRawTo8bit = res[-1].split("=", 1)[-1].strip()
            if convertRawTo8bit == "no":
                convertRawTo8bit = 0
            else:
                convertRawTo8bit = 1
        pulp["convertRawTo8bit"] = convertRawTo8bit

        # skip RFI zapping?
        res = [ii for ii in loglines if "RFI" in ii]
        if len(res) == 0:
            skipRFIExcision = 0
        else:
            skipRFIExcision = res[-1].split("=", 1)[-1].strip()
            if skipRFIExcision == "yes":
                skipRFIExcision = 0
            else:
                skipRFIExcision = 1
        pulp["skipRFIExcision"] = skipRFIExcision

        # skip folding?
        res = [ii for ii in loglines if "No folding" in ii]
        if len(res) == 0:
            skipDataFolding = 0
        else:
            skipDataFolding = 1
        pulp["skipDataFolding"] = skipDataFolding

        # skip pdmp?
        res = [ii for ii in loglines if "pdmp" in ii]
        if len(res) == 0:
            skipOptimizePulsarProfile = 0
        else:
            skipOptimizePulsarProfile = res[-1].split("=", 1)[-1].strip()
            if skipOptimizePulsarProfile == "yes":
                skipOptimizePulsarProfile = 0
            else:
                skipOptimizePulsarProfile = 1
        pulp["skipOptimizePulsarProfile"] = skipOptimizePulsarProfile

        # skip dspsr?
        res = [ii for ii in loglines if "DSPSR" in ii and "DAL" not in ii]
        if len(res) == 0:
            skipConvertRawIntoFoldedPSRFITS = 0
        else:
            skipConvertRawIntoFoldedPSRFITS = res[-1].split("=", 1)[-1].strip().split(",", 1)[0].strip()
            if skipConvertRawIntoFoldedPSRFITS == "yes":
                skipConvertRawIntoFoldedPSRFITS = 0
            else:
                skipConvertRawIntoFoldedPSRFITS = 1
        pulp["skipConvertRawIntoFoldedPSRFITS"] = skipConvertRawIntoFoldedPSRFITS

        # perform RRAT analysis?
        res = [ii for ii in loglines if "RRATs" in ii]
        if len(res) == 0:
            runRotationalRAdioTransientsAnalysis = 0
        else:
            runRotationalRAdioTransientsAnalysis = res[-1].split("=", 1)[-1].strip()
            if runRotationalRAdioTransientsAnalysis == "yes":
                runRotationalRAdioTransientsAnalysis = 1
            else:
                runRotationalRAdioTransientsAnalysis = 0
        pulp["runRotationalRAdioTransientsAnalysis"] = runRotationalRAdioTransientsAnalysis

        # skip prepfold?
        res = [ii for ii in loglines if "Prepfold" in ii]
        if len(res) == 0:
            skipPrepfold = 0
        else:
            skipPrepfold = res[-1].split("=", 1)[-1].strip()
            if skipPrepfold == "yes":
                skipPrepfold = 0
            else:
                skipPrepfold = 1
        pulp["skipPrepfold"] = skipPrepfold

        # skip dynamic spectrum?
        res = [ii for ii in loglines if "Subdyn" in ii]
        if len(res) == 0:
            skipDynamicSpectrum = 0
        else:
            skipDynamicSpectrum = res[-1].split("=", 1)[-1].strip().split(" ", 1)[0].strip()
            if skipDynamicSpectrum == "yes":
                skipDynamicSpectrum = 0
            else:
                skipDynamicSpectrum = 1
        pulp["skipDynamicSpectrum"] = skipDynamicSpectrum

        # pulp start, stop times and wall time
        res = [ii for ii in loglines if "UTC" in ii and "Start" not in ii]
        try:
            st = res[-2].split("is:", 1)[-1].strip()
        except:
            print("(E) Can't extract the pipeline start time for SASId%s" % (sasid))
            sys.exit(1)

        # Start Time
        starttime = datetime.strptime(st, '%a %b  %d %H:%M:%S %Y').strftime('%Y-%m-%d %H:%M:%S')
        pt = datetime.strptime(st, '%a %b  %d %H:%M:%S %Y')
        total_seconds = pt.timestamp() # since 01.01.1970
        # End Time
        et = res[-1].split("is:", 1)[-1].strip()
        endtime = datetime.strptime(et, '%a %b  %d %H:%M:%S %Y').strftime('%Y-%m-%d %H:%M:%S')
        pt = datetime.strptime(et, '%a %b  %d %H:%M:%S %Y')
        # duration
        duration = pt.timestamp() - total_seconds
        pulp["Start Time"] = starttime
        pulp["Duration [s]"] = duration
        pulp["End Time"] = endtime

        # Pipeline Name
        res = [ii for ii in loglines if "Target" in ii]
        target = res[0].split("Target: ")[1].split(" ")[0].strip()
        pulp["Pipeline Name"] = target + "/PULP"

    else:  # for old-type log-files *_combined.log

        pulp["doSinglePulseAnalysis"] = 0 # no single-pulse processing in the old pipelines
        pulp["runRotationalRAdioTransientsAnalysis"] = 0 # no RRATs processing in the old pipelines

        # convert raw data to 8 bit?
        res = [ii for ii in loglines if "digitize.py" in ii]
        if len(res) == 0:
            convertRawTo8bit = 0
        else:
            convertRawTo8bit = 1
        pulp["convertRawTo8bit"] = convertRawTo8bit

        # skip RFI zapping?
        res = [ii for ii in loglines if "rfifind" in ii]
        if len(res) == 0:
            skipRFIExcision = 1
        else:
            skipRFIExcision = 0
        pulp["skipRFIExcision"] = skipRFIExcision

        # skip pdmp?
        res = [ii for ii in loglines if "pdmp" in ii]
        if len(res) == 0:
            skipOptimizePulsarProfile = 1
        else:
            skipOptimizePulsarProfile = 0
        pulp["skipOptimizePulsarProfile"] = skipOptimizePulsarProfile #pdmp

        # skip dspsr?
        res = [ii for ii in loglines if "dspsr" in ii and "Running:" in ii]
        if len(res) == 0:
            skipDspsr = 1
        else:
            skipDspsr = 0
        pulp["skipConvertRawIntoFoldedPSRFITS"] = skipDspsr #dspsr

        # skip prepfold?
        res = [ii for ii in loglines if "prepfold" in ii and "Running:" in ii]
        if len(res) == 0:
            skipPrepfold = 1
        else:
            skipPrepfold = 0
        pulp["skipPrepfold"] = skipPrepfold

        # no folding?
        if skipPrepfold == 0 or skipDspsr == 0:
            skipDataFolding = 0
        else:
            skipDataFolding = 1
        pulp["skipDataFolding"] = skipDataFolding

        # skip dynamic spectrum?
        res = [ii for ii in loglines if "subdyn.py" in ii]
        if len(res) == 0:
            skipDynamicSpectrum = 1
        else:
            skipDynamicSpectrum = 0
        pulp["skipDynamicSpectrum"] = skipDynamicSpectrum

        # pulp start, stop times and wall time
        res = [ii for ii in loglines if "Start date:" in ii]
        try:
            st = res[0].split("date: ", 1)[-1].strip()
        except:
            print("(E) Can't extract the pipeline start time for SASId%s" % (sasid))
            sys.exit(1)

        # Start Time
        starttime = datetime.strptime(st, '%a %b  %d %H:%M:%S UTC %Y').strftime('%Y-%m-%d %H:%M:%S')
        pt = datetime.strptime(st, '%a %b  %d %H:%M:%S UTC %Y')
        total_seconds = pt.timestamp() # since 01.01.1970

        res = [ii for ii in loglines if "End Time:" in ii]
        try:
            et = res[0].split("Time: ", 1)[-1].strip()
            print (et)
        except:
            print("(E) Can't extract the pipeline stop time for SASId%s" % (sasid))
            sys.exit(1)

        # End Time
        endtime = datetime.strptime(et, '%a %b  %d %H:%M:%S UTC %Y').strftime('%Y-%m-%d %H:%M:%S')
        pt = datetime.strptime(et, '%a %b  %d %H:%M:%S UTC %Y')
        # duration
        duration = pt.timestamp() - total_seconds
        pulp["Start Time"] = starttime
        pulp["Duration [s]"] = duration
        pulp["End Time"] = endtime

        # Pipeline Name
        res = [ii for ii in loglines if "Pulsar name:" in ii]
        target = res[0].split(" ")[-1].strip()
        if len(target) == 0:
            res = [ii for ii in loglines if "Main PULSAR" in ii]
            target = res[0].split(" ")[-1].strip()
        pulp["Pipeline Name"] = target + "/PULP"

    # for all log-files
    pulp["Pipeline Version"] = "n/a"
    pulp["Process Identifier Name"] = pulp["Pipeline Name"]
    pulp["Strategy Name"] = "Pulsar Pipeline"
    pulp["Pulsar Selection"] = "Pulsars in observation specs, file or SAP"
    pulp["Strategy Description"] = "default"
    pulp["subIntegrationLength [s]"] = -1.0
    pulp["Pulsars"] = 0

    return pulp
