#!/usr/bin/env python
#
# Fixig the duplicate tarball problem is the early BF LTA tarballs 
#
# (c) Vlad Kondratiev - 22.07.2021 
#
# 17.08.2021 - added option for the output tarball name
#
# 30.08.2021 - writing out the file content of the output tarball
#              in the ascii file.
#              Also, changing the old tree structure in the tarball, removing
#              top _CVplots, _red_locus*, etc directories
#
import fnmatch
import json
import optparse as opt
import os
import re
import shutil
import sys
import tarfile

# files that are not in the duplicate tarballs but we still need to keep them
excluded = ["*.log", "*.out", "*.par", "*.ps", \
            "*.txt", "*.png", "*.h5", "*.raw", "*.parset", \
            "*.posn", "*.per", "*.ar", "*.AR", "*.pfd", "*.pfd.*", "*out", "*.inf", "*.singlepulse", "*.dat", \
            "*rep", "*.sh", "*.pdf", "*.list", "*.fits", "*.fil", "*_rfifind.*", "*_nomask", \
            "*_sp_singlepulse.tar.gz", "*_sp_inf.tar.gz"]

# files that we definitety want to remove even if they fall into excluded list
# these are the files that were manually created by users who had access to CEP1/2
# these could be BOTH regular files and directories
included = ["*tz.tmp", "*tz.in", "*tempo.lis", "*.kimon", "*.flux.*", "*kimon*", \
            "*bws*", "*ben*", "*stappers*", "*vlad*", "*joeri*", "*jason*", "*jwth*", "*jvl*", "*aris*", \
            "*patrick*", "*weltevrede*", "*kondratiev*", "*maura*", "*pilia*", "*tom*", "*hassall*", "*zagkouris*", \
            "*anya*", "*bilous*", "*joris*", "*verbiest*", \
            "*test_*", "*_test*", "*test-*", "*-test*", "*test.*", "*.test*", \
            "*newper*", "*newdm*", "*newpar*", "*best.*", "*best_*", "*best-*", "*.coord.*", \
            "*.coord_*", "*.newcoord.*", "*.newcoord_*", "*.b8.*", "*.b8_*", \
            "*.pazi", "*.pazi.*", "*.pazi_*", "*.pazi2.*", "*.pazi2_*", "*.tst.fits", \
            "*pgplot.ps", "*.ded", "*.cordm.*", "*_cleaned.*", "*_cleaned_*", "*.cleaned.*", "*.cleaned_*", \
            "*.pT.*", "*_0000.*", "*.??-??.ar", "*_NEW.*", "*_NEW_*", "*.low.*", "*.high.*", "*make-cv-diag.sh"]

# patterns for subdirectories that have to be removed from the tarball file
# The "pattern" should not have *, ?, etc characters, and be encompassed by the "/" symbols
# Only for the tarballs that need restructuring, i.e. they have the top-level root-directory. This subdirectory should ALWAYS be the 
# next level subdirectory to be removed.
subdirs_to_remove=["/sp/", "sp/"]

SIZE_FACTORS = ["B", "kB", "MB", "GB", "TB", "PB", "EB"]


def size_to_string(size: int):
    for factor_index, label in enumerate(SIZE_FACTORS):
        factor = 1024 ** factor_index
        next_factor = 1024 * factor
        if size < next_factor:
            return "{:.2f} {}".format(size / factor, label).strip()
    else:
        return None


def comparison_summary(sasid, input_path, output_path, to_delete, to_extract):
    ldv_summary = "{}-ldv-summary.json".format(sasid)
    summary = {}
    input_size, output_size = map(os.path.getsize, (input_path, output_path))
    ratio = output_size / input_size
    summary['input_name'] = input_path.split('/')[-1]
    summary['output_name'] = output_path.split('/')[-1]
    summary['input_size'] = input_size
    summary['output_size'] = output_size
    summary['input_size_str'] = size_to_string(input_size)
    summary['output_size_str'] = size_to_string(output_size)
    summary['size_ratio'] = ratio
    summary['deleted'] = to_delete
    summary['added'] = to_extract
    with open(ldv_summary, 'w') as fp:
        json.dump(summary, fp)


# writing out the summary comparison between old and new tarballs
def formatted_comparison_summary(sasid, input_tarball, new_output_name, to_delete,
                                 to_extract, dirprefix, oldtarcontent, newtarcontent):

    in_filename = input_tarball.split('/')[-1]
    out_filename = new_output_name.split('/')[-1]

    ldv_summary = "%s-ldv-summary.log" % (sasid,)
    fsum = open(ldv_summary, "w")
    fsum.write("*"*98+"\n")
    fsum.write(("  " + " | ".join([in_filename, out_filename])+ "  ").center(98, "*") + "\n")
    fsum.write("*"*98+"\n")

    input_size = os.path.getsize(input_tarball)
    output_size = os.path.getsize(new_output_name)
    ratio = (float)(output_size) / input_size
    factors = {10: "kB", 20: "MB", 30: "GB", 40: "TB"}
    for ii in factors.keys():
        if (float)(input_size) / 2 ** ii < 1:
            break
        else:
            input_factor = ii
    for ii in factors.keys():
        if (float)(output_size) / 2 ** ii < 1:
            break
        else:
            output_factor = ii
    fsum.write("=" * 75 + "\n")
    fsum.write("Old name: %s\n" % (in_filename))
    fsum.write("New name: %s\n" % (out_filename))
    fsum.write("=" * 75 + "\n")
    fsum.write("Old size: %15ld bytes (%5.1f %s)\n" % (
        input_size, (float)(input_size) / 2 ** input_factor, factors[input_factor]))
    fsum.write("New size: %15ld bytes (%5.1f %s)\n" % (
        output_size, (float)(output_size) / 2 ** output_factor, factors[output_factor]))
    fsum.write("Size Ratio (new/old): %.3f\n" % (ratio))
    fsum.write("=" * 75 + "\n")
    fsum.write("Files to delete from the old tarball (%d):\n" % (len(to_delete)))
    for ii in to_delete:
        fsum.write(ii + "\n")
    fsum.write("=" * 75 + "\n")
    fsum.write("Files to add to the new tarball (%d):\n" % (len(to_extract)))
    for ii in to_extract:
        fsum.write(ii + "\n")
    fsum.write("=" * 75 + "\n")
    fsum.write("File contents of the old tarball (%d):\n" % (len(oldtarcontent)))
    fsum.write("=" * 75 + "\n")
    for ii in oldtarcontent:
        fsum.write(ii + "\n")
    fsum.write("=" * 75 + "\n")
    fsum.write("File contents of the new tarball (%d):\n" % (len(newtarcontent)))
    fsum.write("=" * 75 + "\n")
    for ii in newtarcontent:
        fsum.write(ii + "\n")
    fsum.close()


# main
def main():
    usage = "Usage: %prog [options] <input LTA tarball> <output tarball>"
    cmdline = opt.OptionParser(usage)
    # adding options
    # cmdline.add_option('-o', '--output', dest='outfile', metavar='OUTPUT TARBALL NAME', help='If not given, \
    # then "_new" suffix will be added to the name of the input tarball', default="", type='str')

    # reading cmd options
    (opts, args) = cmdline.parse_args()

    # check if input file is given
    if len(args) == 0:
        cmdline.print_usage()
        sys.exit(0)

    # input LTA tarball
    input_tarball = args[0]

    # output tarball
    if len(args) < 2:
        print("The name of the output tarball is not given!")
        sys.exit(1)

    output_tarball = args[1]

    # Before doing anything else we will delete the top-level sub-directories, given by subdirs_to_remove
    # otherwise it can take ages to process  the tarball that have thousands of small files (like was the use case for L102418 with sp/
    # directories
    topleveldirfile = ".tmp.topleveldir.txt"
    os.system("tar -tf %s | head -1 | sed -e 's/\/.*//' > %s" % (input_tarball, topleveldirfile))
    with open(topleveldirfile, 'r') as f:
        topleveldir = f.read().splitlines()[0]
    os.remove(topleveldirfile)
    print ("Removing the following subdirs, if exist:")
    for ii in subdirs_to_remove:
        sd=topleveldir + ii[:-1]
        print (sd),
        try:
            os.system("tar -f %s --delete %s" % (input_tarball, sd))
            print ("- deleted!")
        except:
            print ("- not found")

    # getting all *.tar.gz in the LTA tarball
    matches = []
    inputtar = tarfile.open(input_tarball, "r")
    dircontent = [ii.name for ii in inputtar.getmembers()]
    for filename in fnmatch.filter(dircontent, '*.tar.gz'):
        if "_sp_singlepulse.tar.gz" not in filename and "_sp_inf.tar.gz" not in filename:
            matches.append(filename)
    inputtar.close()

    ascii_name = output_tarball + "_filecontent.txt"
    with open(ascii_name, "w") as outfile:
        outfile.write("\n".join(dircontent))

    # checking whether _all_ files in dircontent have the same prefix dirname in the path
    dirprefix = dircontent[0].split("/")[0]
    for ii in dircontent:
        if dirprefix != ii.split("/")[0]:
            dirprefix = ""
            break

    # checking if dirprefix has any one of these patterns: "_CVplots", "_CSplots", "_ISplots", "_redIS", "_red_locus"
    # if this is the case, then we will create a new tarball with this prefix removed
    is_restructured = False
    pattern = re.compile(
        r"(_CVplots)|(_CSplots)|(_ISplots)|(_redIS)|(_red_locus)|(_red)")
    if pattern.search(dirprefix) is not None:  # i.e. we need to rewrite the tarball
        is_restructured = True
        inputtar = tarfile.open(input_tarball, "r")
        outputtar = tarfile.open(output_tarball, "w")
        for member in inputtar.getmembers():
            obj = inputtar.extractfile(member)
            member.name = member.name.split("%s/" % (dirprefix))[-1]
            outputtar.addfile(member, obj)
        outputtar.close()
        inputtar.close()
    else:  # making the tarball copy
        shutil.copyfile(input_tarball, output_tarball)

    # removing dirprefix for all files in dircontent
    oldtarcontent = dircontent.copy()
    if dirprefix != "":
        dircontent = [ii.split("%s/" % (dirprefix))[-1] for ii in dircontent]

    # merging contents of all tarballs together in a single list
    flist = []
    to_delete = []
    inputtar = tarfile.open(input_tarball, "r")
    for ff in matches:
        tgz = inputtar.extractfile(ff)
        try:
            with tarfile.open(ff, "r:gz", fileobj=tgz) as tar:
                try:
                    for ii in tar.getmembers():
                        if ii.name[0:2] == "./":
                            flist.append(ii.name[2:])
                        else:
                            flist.append(ii.name)
                except:
                    try:
                        for ii in tar.getnames():
                            if ii[0:2] == "./":
                                flist.append(ii[2:])
                            else:
                                flist.append(ii)
                    except:
                        try:
                            with open('.tmp.intrinsic.txt', 'w') as sys.stdout:
                                tar.list(verbose=False, members=None)
                        except:
                            pass
                        finally:
                            sys.stdout = sys.__stdout__
                            with open('.tmp.intrinsic.txt', 'r') as f:
                                for line in f:
                                    line = line.strip()
                                    if line[0:2] == "./":
                                        flist.append(line[2:])
                                    else:
                                        flist.append(line)
                            os.remove('.tmp.intrinsic.txt')
        except:
            to_delete.append(ff)

    # keeping only unique lines
    if len(flist) != 0:
        flist = list(set(flist))
    else:
        flist = dircontent

    # cross-checking files in the LTA tarball with files in the internal *.tar.gz files
    # extra files will be added to to_delete list for removal
    # if flist is empty (could be due to corrupted intrinsic tarballs), then we have a failsafe with putting files that we need in 'excluded' list
    for ff in dircontent:
        is_excluded = []
        for jj in excluded:
            is_excluded.extend(fnmatch.filter([ff], jj))
        if len(is_excluded) != 0: continue
        if ff not in flist:
            to_delete.append(ff)

    # adding files for deletion that really need to go
    for ff in dircontent:
        for jj in included:
            if fnmatch.filter([ff], jj):
                to_delete.append(ff)

    # keeping only unique lines
    to_delete = list(set(to_delete))

    # getting files that are possibly only in the internal *.tar.gz, but not in the LTA tarball.
    # these files will be extracted and kept
    to_extract = list(set(flist) - set(flist).intersection(set(dircontent)))
    if len(to_extract) == 0:
        print("No extra files to extract from *.tar.gz")
    else:
        # first we need to check again if any of these extra files match the filter to be defnitely removed
        to_remove = []
        for ff in to_extract:
            for jj in included:
                if fnmatch.filter([ff], jj):
                    to_remove.append(ff)
        to_extract = [ff for ff in to_extract if ff not in to_remove]

        # opening output tarball for writing
        outputtar = tarfile.open(output_tarball, "a")
        print("To extract:")
        # if we have only one internal *.tar.gz (usually)
        if len(matches) == 1:
            jj = inputtar.extractfile(matches[0])
            try:
                tar = tarfile.open(matches[0], "r:gz", fileobj=jj)
                for ii in to_extract:
                    print(ii)
                    # !!! here we should extract from an internal tgz into the LTA tarball !!!
                    # if dirprefix != "", then it should be added also for the extracted file
                    try:
                        tar.extract("%s/%s" % (dirprefix, ii), './')
                    except:
                        try:
                            tar.extract("%s" % (ii), './')
                        except: pass
                    # giving the same dirprefix as in the original tarball
                    if is_restructured:
                        outputtar.add(ii, "%s" % (ii))
                    else:
                        outputtar.add(ii, "%s/%s" % (dirprefix, ii))
                    # removing extracted file
                    os.remove(ii)
                tar.close()
            except: pass
        else:
            # if we have several *.tar.gz we need first to know which tarball this file belong to
            for ii in to_extract:
                print(ii, "[from: ", end='')
                for tgz in matches:
                    jj = inputtar.extractfile(tgz)
                    try:
                        with tarfile.open(tgz, "r:gz", fileobj=jj) as tar:
                            if ii in [uu.name for uu in tar.getmembers()]:
                                print(tgz, "]")
                                # !!!! here we should extract from an internal tgz into the LTA tarball !!!
                                # if dirprefix != "", then it should be added also for the extracted file
                                try:
                                    tar.extract("%s/%s" % (dirprefix, ii), './')
                                except:
                                    try:
                                        tar.extract("%s" % (ii), './')
                                    except: pass
                                # giving the same dir prefix as in the original tarball
                                if is_restructured:
                                    outputtar.add(ii, "%s" % (ii))
                                else:
                                    outputtar.add(ii, "%s/%s" % (dirprefix, ii))
                                # removing extracted file
                                os.remove(ii)
                                break
                    except: pass
        outputtar.close()

    inputtar.close()

    # delete list should also include internal *.tar.gz
    if dirprefix != "":
        if not is_restructured:
            to_delete = [dirprefix + "/" + ii for ii in to_delete]
    if len(to_delete) != 0:
        print("To delete: ")
    for ii in to_delete:
        print(ii)
        # Python tarfile module does not allow to delete files from the archives, using system calls for now
        os.system("tar -vf %s --delete %s" % (output_tarball, ii))

    # actually here we have to create our own new tarball name following the current naming scheme we use for BF PULP data
    new_output_name = ""
    filename = os.path.basename(input_tarball)
    if "LOFAR_PULSAR_ARCHIVE" in filename:
        sasid = filename.split("_")[4]
        tartype = filename.split("_")[5]
    else:
        sasid = filename.split("_")[0]
        tartype = filename.split("_")[1]

    if "summaryCV" in tartype or "CVplots" in tartype:
        new_output_name = "%s_summaryCV.tar" % (sasid)
    if "summaryCS" in tartype or "CSplots" in tartype:
        new_output_name = "%s_summaryCS.tar" % (sasid)
    if "summaryIS" in tartype or "redIS" in tartype:
        new_output_name = "%s_summaryIS.tar" % (sasid)

    # if output filename is not yet decided (i.e. our tarball is not for summaries)
    if new_output_name == "":
        if "SAP" in tartype:  # then the filename is new already
            (sasid, sap, beam, part, rest) = filename.split("_")
            new_output_name = "%s_%s_%s_%s_bf.tar" % (sasid, sap, beam, part)
        if "red" in tartype:
            logf = ""
            for filename in sorted(fnmatch.filter(dircontent, '*.log')):
                if "sap" in filename and "beam" in filename:
                    logf = filename
                    break
            if logf != "":
                if "part" not in logf:
                    partint = 0
                    partstr = "P000"
                pieces = logf.split("_")
                for pp in pieces:
                    if "sap" in pp:
                        sapint = int(pp.split("sap")[1])
                        sapstr = "SAP%03d" % (sapint)
                    if "beam" in pp:
                        beamint = int(pp.split("beam")[1].split(".")[0])
                        beamstr = "B%03d" % (beamint)
                    if "part" in logf:
                        if "part" in pp:
                            partint = int(pp.split("part")[1].split(".log")[0])
                            partstr = "P%03d" % (partint)
                new_output_name = "%s_%s_%s_%s_bf.tar" % (
                    sasid, sapstr, beamstr, partstr)
            else:  # logfile is not found
                for filename in sorted(fnmatch.filter(dircontent, '*.h5') + fnmatch.filter(
                        dircontent, '*.raw') + fnmatch.filter(dircontent,
                                                              '*.fits') + fnmatch.filter(
                        dircontent, '*.ar') + fnmatch.filter(dircontent, '*.pfd*')):
                    if "_SAP" in filename and "_B" in filename:
                        logf = filename
                        break
                if logf != "":
                    if "_P" not in logf:
                        partint = 0
                        partstr = "P000"
                    pieces = logf.split('/')[-1].split("_")
                    for pp in pieces:
                        if "SAP" in pp:
                            sapint = int(pp.split("SAP")[1])
                            sapstr = "SAP%03d" % (sapint)
                        if "B" in pp:
                            beamint = int(pp.split("B")[1])
                            beamstr = "B%03d" % (beamint)
                        if "_P" in logf:
                            if "P" in pp:
                                partint = int(pp.split("P")[1])
                                partstr = "P%03d" % (partint)
                    new_output_name = "%s_%s_%s_%s_bf.tar" % (
                        sasid, sapstr, beamstr, partstr)
                else: # logfile is not found again (possible reason is that it is VERY early data with 'RSP' in the file names
                    for filename in sorted(fnmatch.filter(dircontent, '*.out')):
                        if "2bf2fits_" in filename:
                            logf = filename
                            break
                    if logf != "":
                        # extracting this found logfile in order to get info
                        outputtar = tarfile.open(output_tarball, "r")
                        members = outputtar.getmembers()
                        log_to_delete=""
                        for ii in members:
                            if "2bf2fits_" in ii.name and ".out" in ii.name:
                                outputtar.extract(ii, "./")
                                log_to_delete = ii.name
                                break
                        outputtar.close()
                        # reading log-file file into the list of lines
                        f = open(logf, 'r')
                        # ignoring empty lines
                        # comments_and_empty=re.compile(r"(^\s*#+.*$)|(^\s*$)")
                        empty = re.compile(r"(^\s*$)")
                        loglines = [ff for ff in f.read().splitlines() if empty.search(ff) is None]
                        f.close()
                        if log_to_delete != "":
                            os.remove(log_to_delete)
                        # look for the line for the specific SAP and TAB
                        res = [ii for ii in loglines if "_bf.raw" in ii]
                        rawfile = res[0].split("/")[-1].split(" ")[0].strip()
                        if "_P" not in rawfile:
                            partint = 0
                            partstr = "P000"
                        pieces = rawfile.split("_")
                        for pp in pieces:
                            if "SAP" in pp:
                                sapint = int(pp.split("SAP")[1])
                                sapstr = "SAP%03d" % (sapint)
                            if "B" in pp:
                                beamint = int(pp.split("B")[1])
                                beamstr = "B%03d" % (beamint)
                            if "_P" in rawfile:
                                if "P" in pp:
                                    partint = int(pp.split("P")[1])
                                    partstr = "P%03d" % (partint)
                        new_output_name = "%s_%s_%s_%s_bf.tar" % (
                            sasid, sapstr, beamstr, partstr)
                    else:
                        print("FATAL ERROR: can't find the information about the SAP and BEAM for this tarball: %s" % (input_tarball))
                        sys.exit(1)
                    
    # renaming the output tarball if needed
    if new_output_name != output_tarball:
        os.rename(output_tarball, new_output_name)

    # making the list of filecontent of the new tarball and save it in the separate ascii file
    outputtar = tarfile.open(new_output_name, "r")
    members = outputtar.getmembers()
    newtarcontent = [ii.name for ii in members]
    # and extracting diagnostic plots
    for ii in members:
        if "status.png" in ii.name or ii.name == "combined.png" or ".pfd.png" in ii.name or \
                "_rfifind.ps" in ii.name or "_diag.png" in ii.name or "_diag_pdmp.png" in ii.name or (
                "heatmap" in ii.name and ".png" in ii.name and not "th.png" in ii.name) or (
                "singlepulse" in ii.name and ".png" in ii.name and not ".ps" in ii.name) or \
                "_pdmp.ps" in ii.name:
            outputtar.extract(ii, "./")
            # if file is Postscript file we will be converting it to PNG (2 pngs in case of rfifind.ps as it's 2-page PS)
            if ".ps" in ii.name:
                os.system(
                    "convert -rotate 90 -background white -alpha remove -alpha off %s %s" % (
                        ii.name, ii.name.split(".ps")[0] + ".png"))
                os.remove(ii.name)
    # and extracting snippets of the log info from rfifind runs
    is_rfilog_found = False
    for ii in members:
        if ".rfiout" in ii.name:
            outputtar.extract(ii, "./")
            rfiname = ii.name.split(".rfiout")[0] + ".rfifind.log"
            os.rename(ii.name, rfiname)
            is_rfilog_found = True
            continue
    if not is_rfilog_found:
        for ii in members:
            if ".log" in ii.name:
                outputtar.extract(ii, "./")
                # reading log-file file into the list of lines
                f = open("%s" % (ii.name), 'r')
                # ignoring empty lines
                # comments_and_empty=re.compile(r"(^\s*#+.*$)|(^\s*$)")
                empty = re.compile(r"(^\s*$)")
                loglines = [ff for ff in f.read().splitlines() if
                            empty.search(ff) is None]
                f.close()
                # does this log-file has any info about rfifind?
                matches = [ff for ff in loglines if 'rfifind' in ff]
                rfiname = ""
                if len(matches) == 1 and "*_rfifind*" in matches[0]:
                    os.remove(ii.name)
                    continue
                rfiname = ""
                if len(matches) != 0:
                    try:
                        rfiname = ii.name.split(".log")[0] + ".rfifind.log"
                        rfiout = open(rfiname, 'w')
                        ind1 = \
                            [i for i, s in enumerate(loglines) if
                             'Creating RFI mask' in s][
                                0]
                        ind2 = ind1 + 2
                        rfiout.write("\n".join(loglines[ind1:ind2]))
                        ind1 = [i for i, s in enumerate(loglines) if
                                'Waiting for rfifind to finish' in s][0]
                        ind2 = [i for i, s in enumerate(loglines) if
                                '(rfifind) has finished' in s][0]
                        ind2 += 1
                        rfiout.write("\n".join(loglines[ind1:ind2]))
                        rfiout.close()
                    except:
                        rfiout.close()
                        os.remove(rfiname)
                    os.remove(ii.name)
                    continue
                else:
                    os.remove(ii.name)
    outputtar.close()

    os.remove(output_tarball + "_filecontent.txt")
    ascii_name = new_output_name + "_filecontent.txt"
    with open(ascii_name, "w") as outfile:
        outfile.write("\n".join(newtarcontent))

    # writing out the summary comparison between old and new tarballs
    comparison_summary(sasid, input_tarball, new_output_name, to_delete,
                       to_extract)

    formatted_comparison_summary(sasid, input_tarball, new_output_name, to_delete,
                                 to_extract, dirprefix, oldtarcontent, newtarcontent)
if __name__ == '__main__':
    main()
