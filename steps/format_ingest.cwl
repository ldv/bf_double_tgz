#!/usr/bin/env cwl-runner

cwlVersion: v1.2
class: ExpressionTool

requirements:
- class: InlineJavascriptRequirement

inputs:
- id: metadata
  type: Any?
- id: file_content
  type: File
- id: output_name
  type: string
- id: file_name
  type: string
- id: md5sum
  type: string
- id: filesize
  type: long

outputs:
- id: ingest
  type: Any
expression: |
  ${

    var dataProduct = {
      "size": inputs.filesize,
      "_type": inputs.file_name.includes('summary') ? "PulpSummaryDataProduct" : "PulpDataProduct",
      "fileName": inputs.file_name,
      "md5checksum": inputs.md5sum,
      "fileFormat": "PULP",
      "storageWriter": "Unknown",
      "dataProductType": "Pulsar pipeline output",
      "storageWriterVersion": "Unknown"
    }
    if(inputs.metadata){
      var pipelineRun = inputs.metadata
    }else{
      var pipelineRun = {}
    }
    
    pipelineRun['fileContent'] = inputs.file_content
    return { "ingest": {
        "path": inputs.output_name,
        "file_name": inputs.file_name,
        "metadata": {
          "dataProduct": dataProduct,
          "pipelineRun": pipelineRun
        }
      }
    }
  }
