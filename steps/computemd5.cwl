cwlVersion: v1.2
class: CommandLineTool
inputs: 
  - id: archive
    doc: Archive to compute md5 checksum for
    type: File
    inputBinding:
      position: 1

stdout: std.out
outputs:
  - id: md5sum
    doc: MD5 sum
    type: string
    outputBinding:
      glob: "std.out"
      loadContents: true
      outputEval: $(self[0].contents.split(' ')[0])
baseCommand:
  - md5sum
requirements:
  - class: InlineJavascriptRequirement
hints:
  - class: DockerRequirement
    dockerPull: git.astron.nl:5000/ldv/bf_double_tgz:v0.5.7