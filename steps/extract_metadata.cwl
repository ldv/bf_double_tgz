class: CommandLineTool
cwlVersion: v1.2
baseCommand:
  - collect_unspecified_metadata
inputs:
  - id: sas_id
    doc: SAS ID (ObservationID)
    type: string
    inputBinding:
      position: 0
  - id: input_tar
    doc: File name
    type: File
    inputBinding:
      position: 1
  - id: log_root_folder
    doc: The absolute path where the pulp-logs are located
    type: Directory
    inputBinding:
      position: 1
      prefix: '--rootdir'    
outputs:
  - id: output_json
    doc: Resulting json file with metadata of given SAS_ID
    type: Any
    outputBinding:
      glob: 'L$(inputs.sas_id)_unspecified.json'
      loadContents: true
      outputEval: | 
        $(self[0] ? JSON.parse(self[0].contents): null)
requirements:
  - class: InlineJavascriptRequirement
hints:
  - class: DockerRequirement
    dockerPull: git.astron.nl:5000/ldv/bf_double_tgz:v0.5.7
