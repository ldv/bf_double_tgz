id: fetchdata
label: fetch_data
class: CommandLineTool
cwlVersion: v1.1
inputs: 
  - id: surl_link
    type: string
    inputBinding:
      position: 0
    
outputs: 
  - id: tar_archive
    type: File
    outputBinding:
      glob: 'out/*.tar'
baseCommand: 
 - 'bash'
 - 'fetch.sh'
doc: 'Untar a compressed file'
requirements:
  InlineJavascriptRequirement: {}
  InitialWorkDirRequirement:
    listing:
      - entryname: 'fetch.sh' 
        entry: |
          #!/bin/bash
          mkdir out
          cd out
          turl=`echo $1 | awk '{gsub("srm://srm.grid.sara.nl[:0-9]*","gsiftp://gridftp.grid.sara.nl"); print}'`
          file_name=$(inputs.surl_link.split('/').pop())
          echo "Downloading $turl to $file_name"
          globus-url-copy $turl file://$PWD/$file_name
          
          if [[ "$file_name"  == *".gz" ]]
          then
            echo "Uncompressing file $file_name"
            gzip $file_name -c -d > $(inputs.surl_link.split('/').pop().split('_').slice(0,-1).join('_'))
          fi
