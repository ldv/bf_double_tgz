#!/usr/bin/env cwl-runner

cwlVersion: v1.2
class: CommandLineTool

requirements:
- class: InlineJavascriptRequirement
- class: InitialWorkDirRequirement
  listing:
  - entryname: summary.json
    entry: $(inputs.metadata)
  - entryname: script.py
    entry: |
      #!/usr/bin/env python3
      import os
      import sys
      import json
      SIZE_FACTORS = ["B", "kB", "MB", "GB", "TB", "PB", "EB"]
      def size_to_string(size: int):
          for factor_index, label in enumerate(SIZE_FACTORS):
              factor = 1024 ** factor_index
              next_factor = 1024 * factor
              if size < next_factor:
                  return "{:.2f} {}".format(size / factor, label).strip()
          else:
              return None

      files_in = sys.argv[1:]
      file_out_path = sys.argv[1].split('/')[-1]
      sasid = file_out_path.split('-')[0].lstrip('L')
      with open('summary.json', 'r') as fin:
          summaries = json.load(fin)

      total_input = 0
      total_output = 0
      for summary in summaries:
          total_input += summary['input_size']
          total_output += summary['output_size']

      with open(file_out_path, 'w') as fout:
          fout.write('#'*97+'\n')
          fout.write('There are %s tarballs processed for the SASId %s\n' % (len(summaries), sasid))
          fout.write('The total size of all input tarballs: %s \n' % (size_to_string(total_input), ))
          fout.write('The total size of all output tarballs: %s \n' % (size_to_string(total_output), ))
          fout.write('The size ratio (output/input) is: %.2f \n' % (total_output/total_input, ))
          fout.write('#'*97+'\n')
          for file in files_in:
              with open(file, 'r') as fin:
                  fout.write(fin.read())

inputs:
- id: metadata
  type: Any[]
- id: summary_files
  type: File[]
  inputBinding:
    position: 1

outputs:
- id: summary_file
  type: File
  outputBinding:
    glob: $(inputs.summary_files[0].basename)

baseCommand:
- python3
- script.py

hints:
- class: DockerRequirement
  dockerPull: git.astron.nl:5000/ldv/bf_double_tgz:v0.5.7
