cwlVersion: v1.2
class: CommandLineTool
inputs: 
  - id: source_tgz
    doc: Source beam formed tar archive to process
    type: File
    inputBinding:
      position: 1

arguments:
  - valueFrom: $(inputs.source_tgz.basename.replace(/_\w{8}.tar/gm, '.tar'))
    position: 2
    
outputs:
  - id: output_tar
    doc: output tar archive
    type: File
    outputBinding:
      glob: "*.tar"
  - id: summary
    doc: summary
    type: Any
    outputBinding:
      glob: "*-ldv-summary.json"
      loadContents: True
      outputEval: $(JSON.parse(self[0].contents))
  - id: plots
    type: File[]
    outputBinding:
      glob:
       - "*.png"
       - "rawvoltages/SAP*/BEAM*/*.png"
       - "*stokes/SAP*/BEAM*/*.png"
       - "*.rfifind.log"
  - id: summary_file
    type: File
    outputBinding:
      glob:
       - "*-ldv-summary.log"
  - id: file_content
    doc: file_content
    type: File
    outputBinding:
      glob: "*_filecontent.txt"
baseCommand:
  - double_tgz_elimination

hints:
  - class: DockerRequirement
    dockerPull: git.astron.nl:5000/ldv/bf_double_tgz:v0.5.7
requirements:
  - class: InlineJavascriptRequirement