#!/usr/bin/env cwl-runner

cwlVersion: v1.2
class: ExpressionTool

requirements:
- class: InlineJavascriptRequirement

inputs:
- id: summary_psrfits_file
  type: File?
- id: summary_file
  type: File?
- id: plots
  type:
  - type: array
    items:
      type: array
      items: File
- id: summaries
  type: Any[]

outputs:
- id: quality
  type: Any
expression: |
  ${
      var plots = []
      for (var p_idx in inputs.plots) {
         plots = plots.concat(inputs.plots[p_idx])
      }

      if(inputs.summary_file){
        plots.push(inputs.summary_file);
      }
      if(inputs.summary_psrfits_file){
        plots.push(inputs.summary_psrfits_file)
      }

      var summary = {}
      for (var s_idx in inputs.summaries) {
          summary[inputs.summaries[s_idx]['input_name']] = inputs.summaries[s_idx]
      }

      return {
       "quality": {
         "plots": plots,
         "details": {},
         "summary": summary,
         "sensitivity": "N/A",
         "uv-coverage": "N/A",
         "observing-conditions": "N/A"
       }

    }
  }
