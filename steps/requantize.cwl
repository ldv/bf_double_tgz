#!/usr/bin/env cwl-runner

cwlVersion: v1.2
class: CommandLineTool

requirements:
- class: InlineJavascriptRequirement
- class: InitialWorkDirRequirement
  listing:
  - entryname: $(inputs.bf_tar_archive.basename.replace(/_([0-9a-zA-Z]+)\.tar/, ".tar"))
    writable: true
    entry: $(inputs.bf_tar_archive)

inputs:
- id: bf_tar_archive
  type: File
  inputBinding:
    position: 0
    valueFrom: $(self.basename)

outputs:
- id: bf_archive
  doc: output tar archive (modified input tar archive)
  type: File
  outputBinding:
    glob:
    - $(inputs.bf_tar_archive.basename)
- id: summary
  doc: summary
  type: Any
  outputBinding:
    glob: '*-ldv-summary.json'
    outputEval: $(JSON.parse(self[0].contents))
    loadContents: true
- id: plots
  type: File[]
  outputBinding:
    glob:
    - '*/*/SAP*/BEAM*/*.png'
    - '*/*.png'
    - '*.png'
    - 'rawvoltages/SAP*/BEAM*/*.png'
    - '*stokes/SAP*/BEAM*/*.png'
    - '*.rfifind.log'
    - '*stokes/SAP*/BEAM*/*_ldv_psrfits_requantisation.log'
- id: file_content
  doc: file_content
  type: File
  outputBinding:
    glob: '*_filecontent.txt'
- id: summary_file
  type: File
  outputBinding:
    glob: '*-ldv-summary.log'

baseCommand:
- psrfits_requantisation

hints:
- class: DockerRequirement
  dockerPull: git.astron.nl:5000/ldv/bf_double_tgz:v0.5.7
