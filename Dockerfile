FROM debian:bullseye AS BUILD

#RUN apt update && apt install -y build-essential git autoconf libtool tcsh zlib1g-dev libcfitsio-dev && \
#    rm -rf /var/lib/apt/lists/*
RUN apt update && apt install -y build-essential git autoconf libtool tcsh wget zlib1g-dev unzip dos2unix && \
    rm -rf /var/lib/apt/lists/*
#RUN mkdir -p /src && cd /src && wget -t 0 -c https://heasarc.gsfc.nasa.gov/FTP/software/fitsio/c/cfitsio-3.47.tar.gz && \
#    tar xvfz cfitsio-3.47.tar.gz && cd cfitsio-3.47 && \
#    ./configure --prefix=/usr/local --enable-sse2 --enable-ssse3 && \
#    make && \
#    make install
RUN mkdir -p /src/cfitsio-3410 && cd /src/cfitsio-3410 && wget -t 0 -c https://heasarc.gsfc.nasa.gov/FTP/software/fitsio/c/cfit3410.zip && \
    unzip cfit3410.zip && find ./ -type f -exec dos2unix --force {} \; && \
    ./configure --prefix=/usr/local --enable-sse2 --enable-ssse3 && \
    make && \
    make install
RUN mkdir -p /src && cd /src && git clone https://github.com/vkond/ldv-psrfits-requantisation.git && \
    cd ldv-psrfits-requantisation && \
    ./prepare && \
    ./configure --prefix /usr/local/ && \
    make && \
    make install && \
    cp *template* /usr/local/etc

FROM python:3.10-bullseye

#RUN apt update && apt install -y ghostscript libcfitsio9 git imagemagick && rm -rf /var/lib/apt/lists/*
RUN apt update && apt install -y ghostscript git imagemagick && rm -rf /var/lib/apt/lists/*
COPY . /src
RUN cd /src/bf_pulp_utils && pip install .
COPY --from=BUILD /usr/local /usr/local
RUN ldconfig
RUN  sed -i 's+<policy domain="coder" rights="none" pattern="PS" />+<policy domain="coder" rights="read | write" pattern="PS" />+g' /etc/ImageMagick-6/policy.xml
RUN  sed -i 's+<policy domain="coder" rights="none" pattern="PS2" />+<policy domain="coder" rights="read | write" pattern="PS2" />+g' /etc/ImageMagick-6/policy.xml
RUN  sed -i 's+<policy domain="coder" rights="none" pattern="PS3" />+<policy domain="coder" rights="read | write" pattern="PS3" />+g' /etc/ImageMagick-6/policy.xml
RUN  sed -i 's+<policy domain="coder" rights="none" pattern="EPS" />+<policy domain="coder" rights="read | write" pattern="EPS" />+g' /etc/ImageMagick-6/policy.xml

RUN collect_unspecified_metadata --help && \
    double_tgz_elimination --help && \ 
    psrfits_requantisation --help
