# bf_double_tgz

This is a CWL workflow used to remove duplicate 'BeamFormed' TAR-balls from LTA


## bf_pulp_utils
This folder contain python scripts, there are two main excutable scripts

* double_tgz_elimination.py
     Script to compare contents of duplicate tarballs inside an LTA tarball and remove all duplicate files


* collect-unspecified-metadata.py
      Script to collect metadata from pulp-log files, resulting in L<obs_id>_unspecified.json file
      Uses ldv_obs.py and ldv_pulp.py

* ldv_obs.py

* ldv_pulp.py


##  steps
* extract_metadata.cwl
     The extracting of the metadata of log file, execute the workflow step as follows:

  `PATH=$PATH:$PWD/scripts cwltool --debug steps/extract_metadata.cwl --sas_id 227149 --log_root_folder <the/absolute/path/of/the/pulp-logs>`

* double_tgz_elimination.cwl


## workflow
There are two workflows defined.
A graphical overview is given below.

### download_and_run_bf_remove.cwl

This is the main workflow which is called from ATDB-LDV.
The bf_process is actually written in the workflow bf_remove_double_tgz.cwl

![Download and run](download_and_run_bf_remove.png)



### bf_remove_double_tgz.cwl

![Remove double tgz](bf_remove_double_tgz.png)
